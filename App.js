import React, {Component} from 'react';
import { 
  StyleSheet, 
  TouchableOpacity, 
  Text, 
  View,
  TextInput, 
  Image 
} from "react-native";

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: '',
      password: '',
      count: 0,
    };
  }

  changeTextInputUser = text => {
    this.setState({user: text});
  };

  changeTextInputPass = text => {
    this.setState({password: text});
  };

  onPress = () => {
    this.setState({
      count: this.state.count + 1,
    });
  };

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.text}>
          <Text> Login </Text>
        </View>
        
        <View style={[styles.countContainer]}>
          <Image style={styles.img} source={require('./img/logo.png')} />
        </View>

        <Text>Usuario: </Text>
        <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={text => this.changeTextInputUser(text)}
          value={this.state.user}
          />

          <Text>Contraseña: </Text>
          <TextInput
          style={{height: 40, borderColor: 'gray', borderWidth: 1}}
          onChangeText={text => this.changeTextInputPass(text)}
          value={this.state.password}
          />

          <TouchableOpacity style={styles.button} onPress={this.onPress}>
            <Text> Iniciar sesión </Text>
          </TouchableOpacity>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    paddingHorizontal: 10,
  },
  text: {
    alignItems: 'center',
    padding: 10,
  },

  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },

  img:{
    width: 200,
    height: 300,
    }
});
